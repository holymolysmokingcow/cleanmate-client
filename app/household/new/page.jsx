'use client';

import { useState } from 'react';
import { useRouter } from 'next/navigation';

import useLocalStorage from '@src/hooks/useLocalStorage';
import { addUser, addHouse, addHouseMate } from '@src/headers';
import ProfileContainer from '@src/components/user/ProfileContainer';

function NewHousehold() {
  const router = useRouter();

  const [user, setUser] = useState({});
  const [house, setHouse] = useState({});

  const [cleanMate, setCleanMate] = useLocalStorage('cm-app-data', {});

  const handleCreateHouse = async () => {
    const userId = await addUser(user);

    house.user = userId;
    const { _id: houseId, code } = await addHouse(house);

    const roommate = await addHouseMate(userId, houseId, code);

    if (roommate) {
      setCleanMate(roommate);
      router.push('/household/invite');
    } else {
      alert('Something went wrong. Mate not added');
    }
  };

  return (
    <div className="flex min-h-screen min-w-[100vw] flex-col justify-center gap-7 bg-white p-7 text-left">
      <h1 className="flex flex-col items-start text-3xl font-semibold">Create New Household</h1>
      <div>
        <div className="mb-9">
          <label htmlFor="household">Household</label>
          <div className="mt-3 box-border flex h-[3.06rem] w-full shrink-0 items-center gap-[0.81rem] rounded-[10px] bg-neutral-100 px-[0.94rem] py-[0.81rem]">
            <img
              className="relative h-[1.44rem] w-[1.44rem] shrink-0"
              alt="home icon"
              src="/assets/images/avatars/home.svg"
            />
            <input
              required
              id="household"
              placeholder="Household Name"
              className=" w-full bg-neutral-100 outline-none"
              onChange={(e) => setHouse({ name: e.target.value })}
            />
          </div>
        </div>

        <ProfileContainer user={user} setUser={setUser} />

        <button
          onClick={handleCreateHouse}
          className="transition duration-300 ease-in-out active:bg-indigo-700 mb-12 mt-14 flex h-14 w-full flex-col items-center justify-center self-stretch rounded-[30px] bg-primary-blue text-white"
        >
          Create Household
        </button>
      </div>
    </div>
  );
}

export default NewHousehold;
