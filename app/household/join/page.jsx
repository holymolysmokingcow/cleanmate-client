'use client';

import { useState } from 'react';
import { useRouter } from 'next/navigation';
import Image from 'next/image';

import useLocalStorage from '@src/hooks/useLocalStorage';
import { addUser, addHouseMate, fetchHouseByCode } from '@src/headers';
import ProfileContainer from '@src/components/user/ProfileContainer';

const JoinHousehold = () => {
  const router = useRouter();

  const [user, setUser] = useState({});
  const [house, setHouse] = useState(null);
  const [error, setError] = useState(null);

  const [cleanMate, setCleanMate] = useLocalStorage('cm-app-data', {});

  const handleInputInviteLink = async (e) => {
    const link = e.target.value;
    if (!link) return;

    const code = link.split('invite/')[1];
    if (!code) {
      setHouse(null);
      setError('Invalid invite link');
      return;
    }

    const res = await fetchHouseByCode(code);
    setHouse(res);
  };

  const handleJoinHouse = async () => {
    if (!house?._id) {
      setError('Invalid invite link');
      return;
    }

    const userId = await addUser(user);
    const houseId = house?._id;
    const code = house?.code;

    const roommate = await addHouseMate(userId, houseId, code);

    if (roommate) {
      setCleanMate(roommate);
      router.push('/chorewheel');
    } else {
      alert('Something went wrong. Mate not added');
    }
  };

  return (
    <div className="relative flex min-h-screen min-w-[100vw] flex-col justify-center gap-5 bg-white p-7 text-left">
      <h1 className="flex flex-col items-start text-3xl font-semibold">Join Household</h1>

      <div className="shrink-0 self-stretch">
        <div className="-mt-5 mb-1">Invite Link</div>

        <div className="box-border flex h-[3.06rem] w-full shrink-0 items-center gap-[0.81rem] rounded-[10px] bg-neutral-100 px-[0.94rem] py-[0.81rem]">
          <img
            className="relative h-[1.44rem] w-[1.44rem] shrink-0"
            alt="Invite Link Icon"
            src="/assets/images/icons/link.svg"
          />
          <input
            required
            id="invite_link"
            placeholder="Paste Link"
            className="w-full bg-neutral-100 outline-none"
            onBlur={handleInputInviteLink}
          />
        </div>

        <div className="mx-5 my-2">
          {house ? (
            <div className="flex items-center space-x-2 mt-4">
              <Image
                alt="Checkmark Icon"
                src="/assets/images/icons/checkmark.svg"
                width={18}
                height={14}
              />
              <p className="text_17 font-bold">{house.name}</p>
            </div>
          ) : (
            <p className="text_15 text-rose-600 font-normal">{error}</p>
          )}
        </div>
      </div>

      <div className="h-0.5 bg-neutral-300" />

      <div>
        <ProfileContainer user={user} setUser={setUser} />
        <button
          className="mb-12 mt-14 flex h-14 w-full flex-col items-center justify-center self-stretch rounded-[30px] bg-primary-blue text-white"
          onClick={handleJoinHouse}
        >
          Join Household
        </button>
      </div>
    </div>
  );
};

export default JoinHousehold;
