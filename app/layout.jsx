import './globals.css'

export const metadata = {
  title: 'CleanMate',
}

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <body className="body">{children}</body>
    </html>
  )
}
