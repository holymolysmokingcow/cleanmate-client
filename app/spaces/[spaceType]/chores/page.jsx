'use client';

import { useState, useEffect } from 'react';
import Link from 'next/link';
import Image from 'next/image';

import { DAYS } from '@src/constants';
import { fetchSpaceChores } from '@src/headers';
import ChoreList from '@src/components/chore/ChoreList';

function SpaceChores({ params: { spaceType } }) {
  const [space, setSpace] = useState({});
  const [chores, setChores] = useState([]);

  useEffect(() => {
    fetchSpaceChores(spaceType)
      .then((space) => {
        setSpace(space || {});
        setChores(space?.chores || []);
      })
      .catch(() => {});
  }, [spaceType]);

  return (
    <div className="flex flex-col items-center h-scree mt-4">
      <div className="w-full m-4 px-6">
        <Link href="/chorewheel/create" className="float-right">
          <Image alt="Close Icon" src="/assets/images/icons/crossicon.svg" width={18} height={18} />
        </Link>
      </div>

      <div className="flex flex-col items-center space-y-4">
        <Image src={space.avatar} alt={space.title} width={100} height={125} />
        <p className="text-xl font-normal">{space.title} Chores</p>
      </div>

      <div
        className="w-full max-w-md mt-8 py-2 px-4 rounded-2xl"
        style={{ backgroundColor: space.color + '10' }}
      >
        <ChoreList chores={chores} />
      </div>
    </div>
  );
}

export default SpaceChores;
