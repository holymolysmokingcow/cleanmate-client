'use client';

import { useState, useEffect } from 'react';
import Link from 'next/link';
import Image from 'next/image';
import MyChores from '@src/components/tab/MyChores';
import AllChores from '@src/components/tab/AllChores';

const ChoreWheel = () => {
  const [daysLeft, setDaysLeft] = useState();
  const [todaysDate, setTodaysDate] = useState();

  const [choresBtnToggle, setChoresBtnToggle] = useState(true);

  useEffect(() => {
    const date = new Date();

    const days = 7 - date.getDay();
    setDaysLeft(days);

    const options = { weekday: 'long', month: 'short', day: 'numeric' };
    const formattedDate = date.toLocaleString(undefined, options);
    setTodaysDate(formattedDate);
  }, []);

  return (
    <div className="max-w-screen-lg mx-auto bg_cloud">
      <div className="flex flex-col mb-7 pt-10 px-7">
        <div className="flex justify-between items-center mb-9 md:mb-12">
          <div>
            <header className="text-3xl">Chore Wheel</header>
          </div>
          <div>
            <Link href="/chorewheel">
              <Image
                alt="User Avatar"
                src="/assets/images/avatars/useron.svg"
                width={32}
                height={32}
              />
            </Link>
          </div>
        </div>

        <div className="flex justify-between items-center mb-2.5">
          <div>
            <p className="text_15 text-lightgray">Today</p>
            <p className="text_15 text-black">{todaysDate}</p>
          </div>
          <div>
            <p className="text_15 text-lightgray">Rotation</p>
            <div className="flex flex-row items-center space-x-2">
              <Image
                alt="Rotation Icon"
                src="/assets/images/icons/rotationicon.svg"
                width={14}
                height={14}
              />
              <p className="text_15 text-black">Sunday</p>
            </div>
          </div>
        </div>

        <div className="w-full">
          <p className="text-xl font-normal">{daysLeft} Days Left</p>
        </div>
      </div>

      <div className="flex flex-col max-w-xl mx-auto px-4">
        <div className="w-full inline-block">
          <button
            className="w-1/2 rounded-l-full px-4 py-2.5 text_15 font-medium"
            style={{
              backgroundColor: choresBtnToggle ? '#151391' : '#F4F4F4',
              color: choresBtnToggle ? '#FFF' : '#787878',
            }}
            onClick={() => setChoresBtnToggle(true)}
          >
            My Chores
          </button>

          <button
            className="w-1/2 rounded-r-full px-4 py-2.5 text_15 font-medium"
            style={{
              backgroundColor: choresBtnToggle ? '#F4F4F4' : '#151391',
              color: choresBtnToggle ? '#787878' : '#FFF',
            }}
            onClick={() => setChoresBtnToggle(false)}
          >
            All Chores
          </button>
        </div>
      </div>

      <div className="max-w-xl mx-auto px-4">
        <div>{choresBtnToggle ? <MyChores /> : <AllChores />}</div>
      </div>
    </div>
  );
};

export default ChoreWheel;
