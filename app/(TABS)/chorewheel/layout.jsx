import './chorewheel.css'

export const metadata = {
  title: 'ChoreWheel - CleanMate',
}

export default function RootLayout({ children }) {
  return (
    <div>{children}</div>
  )
}
