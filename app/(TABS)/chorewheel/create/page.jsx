import React from 'react';
import Link from 'next/link';
import Image from 'next/image';
import AddSpaceChoreCard from '@src/components/space/AddSpaceChoreCard';

const CreateChoreWheel = () => {
  return (
    <div className="max-w-screen-xl mx-auto p-4 md:scale-90 md:mt-[-75px] lg:mt-[-45px]">
      <div className="flex flex-col space-y-4 mx-2 mt-4">
        <div className="flex flex-row justify-between items-center">
          <div>
            <header className="text-[26px]">Create Chore Wheel</header>
          </div>
          <div>
            <Link href="/chorewheel">
              <Image src="/assets/images/icons/crossicon.svg" width={15} height={15} />
            </Link>
          </div>
        </div>
        <section>
          <div className="flex justify-between">
            <div className="">
              <p className="text-base text-slate-400">Duration</p>
              <p className="text-base">Weekly: 7 Days</p>
            </div>
            <div className="">
              <p className="text-base text-slate-400">Rotation</p>
              <div className="flex flex-row items-center space-x-2">
                <Image src="/assets/images/icons/rotationicon.svg" width={14} height={14} />
                <p className="text-base">Sunday</p>
              </div>
            </div>
          </div>
        </section>
      </div>

      <div className="h-4 mt-2 mb-4 border-b-4 border-[#F0F0F0] rounded-full"></div>

      <div className="mx-2">
        <p className="text-xl font-semibold">Shared Spaces</p>
        <p className="text-base font-normal">Add chores and assign roommates</p>
      </div>

      <div className="grid grid-cols-1 md:grid-cols-2 xl:grid-cols-3 gap-2 md:gap-6 mt-4">
        <AddSpaceChoreCard />
      </div>
    </div>
  );
};

export default CreateChoreWheel;
