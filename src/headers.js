import { API_ENDPOINT } from '@src/constants';

export const addUser = async (data) => {
  const res = await fetch(`${API_ENDPOINT}/users`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data),
  });

  if (!res.ok) {
    alert('Something went wrong (User). Try again');
    return;
  }

  const user = await res.json();

  return user?._id;
};

export const addHouse = async (data) => {
  const res = await fetch(`${API_ENDPOINT}/households`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data),
  });

  if (!res.ok) {
    alert('Something went wrong (House). Try again');
    return;
  }

  const house = await res.json();

  return house;
};

export const addHouseMate = async (user, household, inviteCode) => {
  const res = await fetch(`${API_ENDPOINT}/mates`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ user, household, inviteCode }),
  });

  if (!res.ok) {
    alert('Something went wrong (Mate). Try again');
    return;
  }

  const mate = await res.json();

  return mate || null;
};

export const fetchHouseByCode = async (code) => {
  const res = await fetch(`${API_ENDPOINT}/households/code/${code}`);

  if (!res.ok) return null;

  const house = await res.json();

  return house || null;
};

export const fetchHouseMates = async (houseId) => {
  const res = await fetch(`${API_ENDPOINT}/mates?house=${houseId}`);
  const mates = await res.json();

  return mates || [];
};

export const fetchSharedSpaces = async () => {
  const res = await fetch(`${API_ENDPOINT}/spaces`);
  const spaces = await res.json();

  return spaces || [];
};

export const fetchSpaceChores = async (spaceType) => {
  const res = await fetch(`${API_ENDPOINT}/spaces/${spaceType}/chores`);
  const space = await res.json();

  return space || {};
};

export const fetchMyChores = async (mateId) => {
  const res = await fetch(`${API_ENDPOINT}/mates/${mateId}/chores`);
  const chores = await res.json();

  return chores || [];
};
