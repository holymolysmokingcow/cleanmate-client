'use client';

const ProfileContainer = ({ user, setUser }) => {
  return (
    <div className="shrink-0 self-stretch">
      <label htmlFor="profile-name">Your Profile</label>
      <div className="mb-4 mt-3 box-border flex h-[3.06rem] w-full shrink-0 items-center gap-[0.81rem] rounded-[10px] bg-neutral-100 px-[0.94rem] py-[0.81rem]">
        <img
          className="relative h-[1.44rem] w-[1.44rem] shrink-0"
          alt="user icon"
          src="/assets/images/avatars/user.svg"
        />
        <input
          required
          id="user_name"
          placeholder="Name"
          className="w-full bg-neutral-100 outline-none"
          onChange={(e) => setUser({ ...user, name: e.target.value })}
        />
      </div>
      <div className="box-border flex h-[3.06rem] w-full shrink-0 items-center gap-[0.81rem] rounded-[10px] bg-neutral-100 px-[0.94rem] py-[0.81rem]">
        <img
          className="relative h-[1.75rem] w-[1.75rem] shrink-0"
          alt="phone icon"
          src="/assets/images/avatars/phone.svg"
        />
        <input
          required
          id="user_phone"
          placeholder="Number"
          className="w-full bg-neutral-100 outline-none"
          onChange={(e) => setUser({ ...user, phone: e.target.value })}
          // pattern="/^(\+\d{1,3}[- ]?)?\d{10}$/"
          // title="Enter valid mobile number"
        />
      </div>
    </div>
  );
};

export default ProfileContainer;
