import { useState, useEffect } from 'react';

import { fetchMyChores } from '@src/headers';
import useLocalStorage from '@src/hooks/useLocalStorage';

import MyChoreListItem from '@src/components/tab/MyChoreListItem';

const myChores = [
	{ title: 'Clear and wipe down countertops', frequency: 'weekly', days: ['Tu', 'Fri'] },
	{
		title: 'Wash dish rags and replace sponges',
		frequency: 'As needed',
		notes: ['Start in corner - work backwards', 'Swiffer!', 'Only mop after vacuuming'],
	},
	{
		title: 'Wipe drawers and shelve in fridge and freezer',
		frequency: 'Deep Clean Chore',
		days: ['Tu', 'Fri'],
	},
];

function MyChoreList() {
	const [cleanMate, setCleanMate] = useLocalStorage('cm-app-data');
	const [chores, setChores] = useState(() => myChores);

	// useEffect(() => {
	// 	fetchMyChores(cleanMate?.id)
	// 		.then((chores) => setChores(chores))
	// 		.catch(() => {});
	// }, [cleanMate?.id]);

	return (
		<div className="flex flex-col space-y-2 w-full">
			{chores?.map((chore) => (
				<MyChoreListItem key={chore?._id} chore={chore} />
			))}
		</div>
	);
}

export default MyChoreList;
