import Link from 'next/link';
import Image from 'next/image';
import MyChoreList from '@src/components/tab/MyChoreList';

const space = { title: 'Kitchen', color: '#EAC02C', avatar: '/assets/images/kitchen.svg' };
const chores = [{}, {}, {}];

const MyChores = () => {
  return (
    <div
      className="flex flex-col space-y-4 mt-4 px-[15px] pt-4 pb-10 border-[3px] rounded-xl"
      style={{ backgroundColor: space.color + '10', borderColor: space.color }}
    >
      <div className="flex space-x-3 items-center">
        <div>
          <p className="text-[26px] font-bold">My Chores</p>
        </div>

        <div className="flex justify-center items-center w-6 h-6 bg-primary-blue rounded-lg">
          <p className="text-base text-white font-normal">{chores.length}</p>
        </div>
      </div>

      <div className="flex flex-col items-center space-y-2">
        <p className="text-xl font-normal">{space.title}</p>
        <Image src={space.avatar} alt={space.title} width={79} height={86} />
      </div>

      <div className="flex justify-center">
        <MyChoreList />
      </div>
    </div>
  );
};

export default MyChores;
