import { useState } from 'react';
import Image from 'next/image';

function MyChoreListItem({ chore }) {
	return (
		<div className="flex flex-col space-y-2 px-3.5 py-2.5 bg-white rounded-xl border-2 border-gray">
			<div className="flex space-x-3 items-start">
				<Image
					alt="Check off chore icon"
					src={`/assets/images/icons/squareoff.svg`}
					width={26}
					height={26}
					className="cursor-pointer"
				/>
				<p className="text_17 font-normal">{chore?.title}</p>
			</div>

			<ul className="flex flex-col ml-14">
				{chore?.notes?.map((note) => (
					<li key={note} className="list-disc text_15 font-normal">
						{note}
					</li>
				))}
			</ul>

			<div className="flex flex-col ml-10">
				{chore?.frequency === 'weekly' ? (
					<div className="flex items-center space-x-2">
						{chore?.days?.map((day) => (
							<p key={day} className="text_15 font-normal italic">
								{day},
							</p>
						))}
					</div>
				) : (
					<p className="text_15 font-normal italic">{chore.frequency}</p>
				)}
			</div>
		</div>
	);
}

export default MyChoreListItem;
