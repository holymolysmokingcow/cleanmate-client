import Link from 'next/link';
import Image from 'next/image';
import Wheel from '@src/components/chore/Wheel';

const spaces = [
  { title: 'Kitchen', color: '#EAC02C' },
  { title: 'Living Room', color: '#D926B4' },
  { title: 'Bathroom', color: '#47A7FF' },
];

const AllChores = () => {
  return (
    <div className="flex flex-col space-y-4 mt-4 px-[15px] pt-4 pb-10 border-[3px] border-primary-blue rounded-xl">
      <div>
        <p className="text-2xl font-semibold">All Chores</p>
      </div>

      <div className="flex justify-between md:justify-evenly">
        {spaces.map((space) => (
          <div key={space?._id} class="flex space-x-2 items-center">
            <span className="d_18 rounded-full" style={{ backgroundColor: space.color }} />
            <span className="text_15 font-normal">{space.title}</span>
          </div>
        ))}
      </div>

      <div className="flex justify-center">
        <Wheel />
      </div>
    </div>
  );
};

export default AllChores;
