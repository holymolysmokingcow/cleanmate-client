'use client';

import { useState, useEffect } from 'react';
import Link from 'next/link';
import Image from 'next/image';

import { fetchSharedSpaces } from '@src/headers';
import MateList from '@src/components/mate/MateList';

function AddSpaceChoreCard() {
  const [spaces, setSpaces] = useState([]);

  useEffect(() => {
    fetchSharedSpaces()
      .then((spaces) => setSpaces(spaces))
      .catch(() => {});
  }, []);

  const spaceCard = (space) => (
    <div
      className="flex flex-col justify-center space-y-8 max-w-[470px] mb-8 p-4 border-4 rounded-2xl"
      style={{ borderColor: space.color }}
    >
      <div className="flex justify-center">
        <p className="text-[26px] font-semibold">{space.title}</p>
      </div>

      <div className="flex justify-center">
        <img src={space.avatar} alt={space} className="w-[79px] h-[86px] sm:w-32 sm:h-36" />
      </div>

      <div className="flex justify-center">
        <Link
          href={`/spaces/${space.type}/chores`}
          className="rounded-full py-3 px-5 w-full border-2 border-primary-blue"
        >
          <div className="flex flex-row justify-between">
            <div className="flex items-center space-x-2">
              <Image src="/assets/images/icons/plusonicon.svg" width={24} height={24} />
              <label className="text-base font-semibold">Add Chores</label>
            </div>

            <div className="flex items-center">
              <Image src="/assets/images/icons/rightarrowonicon.svg" width={20} height={20} />
            </div>
          </div>
        </Link>
      </div>

      <div className="flex flex-col space-y-2 px-4">
        <p className="font-semibold">Assign to</p>
        <MateList color={space.color} />
      </div>
    </div>
  );

  return spaces?.map((space) => spaceCard(space));
}

export default AddSpaceChoreCard;
