'use client';

import { useState, useEffect } from 'react';
import Image from 'next/image';

import useLocalStorage from '@src/hooks/useLocalStorage';
import { fetchHouseMates } from '@src/headers';

function MateList({ color }) {
  const [cleanMate, setCleanMate] = useLocalStorage('cm-app-data');

  const [mates, setMates] = useState([]);
  const [selMate, setSelMate] = useState(null);

  useEffect(() => {
    const houseId = cleanMate?.household?._id;

    fetchHouseMates(houseId)
      .then((mates) => setMates(mates))
      .catch(() => {});
  }, [cleanMate]);

  const handleSelMate = ({ _id }) => setSelMate(_id);

  return (
    <div className="flex space-x-6">
      {mates?.map((mate) => (
        <div
          key={mate?._id}
          onClick={() => handleSelMate(mate)}
          className="flex flex-col justify-center space-y-2 w-1/3 aspect-square border-[2.5px] rounded-xl cursor-pointer"
          style={{
            borderColor: selMate === mate?._id ? color : '#D9D9D9',
            backgroundColor: selMate === mate?._id ? '#FFFFFF' : '#F4F4F4',
          }}
        >
          <div className="flex justify-center">
            <Image
              alt={mate?.user?.name}
              src={mate?.user?.avatar}
              width={22}
              height={22}
              className="rounded-full"
            />
          </div>

          <div className="flex justify-center">
            <p className="text-sm font-extralight">{mate?.user?.name?.split(' ')?.[0]}</p>
          </div>
        </div>
      ))}
    </div>
  );
}

export default MateList;
