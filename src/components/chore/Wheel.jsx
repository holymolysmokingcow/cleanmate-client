import { Chart, ArcElement } from 'chart.js';
Chart.register(ArcElement);
import { Doughnut } from 'react-chartjs-2';

const data = {
  labels: ['Kitchen', 'Living Room', 'Bathroom'],
  datasets: [
    {
      data: [1, 1, 1],
      backgroundColor: ['#D926B4', '#47A7FF', '#EAC02C'],
      hoverBackgroundColor: ['#D926B4', '#47A7FF', '#EAC02C'],
      borderWidth: 0,
      cutout: '60%',
    },
  ],
};

function Wheel() {
  return (
    <div className="w-44 h-44">
      <Doughnut data={data} />
    </div>
  );
}

export default Wheel;
