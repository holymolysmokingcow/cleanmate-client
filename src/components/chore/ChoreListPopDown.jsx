import { useState } from 'react';
import Image from 'next/image';

import { DAYS } from '@src/constants';
import ChoreListHeader from './ChoreListHeader';

function ChoreListPopDown({ chore, selChore, handleSelChore }) {
  const [open, setOpen] = useState(false);
  const [freq, setFreq] = useState('weekly');

  return (
    <div className="w-full pb-4 bg-white rounded-xl border-2 border-primary-blue z-10">
      <ChoreListHeader chore={chore} img="plusonicon" handleSelChore={handleSelChore} />

      <div
        className="px-6 py-2 transition-height duration-500 ease-in-out"
        style={{ height: chore?._id === selChore && '100%' }}
      >
        <div className="flex flex-col space-y-2">
          <label className="text-sm text-gray-600 font-normal px-4">Any Notes?</label>
          <textarea
            rows="1"
            name="notes"
            placeholder="Optional"
            className="bg-gray-100 rounded-lg p-4"
          ></textarea>
        </div>

        <div className="flex flex-col space-y-4 mt-4 px-4">
          <label className="text-sm text-gray-600 font-normal">Frequency</label>
          <label for="option1" class="inline-flex items-center">
            <input type="radio" id="option1" name="options" class="form-radio h-[17px] w-[17px]" />
            <span className="ml-2 text-sm text-darkgray font-normal">Deep clean chore</span>
          </label>

          <label for="option2" class="inline-flex items-center">
            <input type="radio" id="option2" name="options" class="form-radio h-[17px] w-[17px]" />
            <span class="ml-2 text-sm text-darkgray font-normal">As needed</span>
          </label>

          <label for="option3" class="inline-flex items-center">
            <input type="radio" id="option3" name="options" class="form-radio h-[17px] w-[17px]" />
            <span class="ml-2 text-sm text-darkgray font-normal">Weekly rotation</span>
          </label>
        </div>

        {freq === 'weekly' && (
          <div className="flex flex-col space-y-2 px-2">
            <div className="flex items-center space-x-3 my-4">
              {DAYS.map((day) => (
                <button
                  type="button"
                  className="bg-white-100 h-8 w-8 border-2 rounded-xl text-xs text-lightgray font-normal"
                >
                  {day}
                </button>
              ))}
            </div>
          </div>
        )}
      </div>
    </div>
  );
}

export default ChoreListPopDown;
