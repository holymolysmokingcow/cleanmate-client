import Image from 'next/image';

function ChoreListHeader({ chore, img, handleSelChore }) {
  return (
    <div className="flex space-x-3 p-4">
      <Image
        src={`/assets/images/icons/${img}.svg`}
        width={26}
        height={26}
        className="cursor-pointer"
        onClick={() => handleSelChore(chore)}
      />
      <p className="text-base font-normal">{chore?.title}</p>
    </div>
  );
}

export default ChoreListHeader;
