import { useState } from 'react';
import Image from 'next/image';
import ChoreListHeader from './ChoreListHeader';
import ChoreListPopDown from './ChoreListPopDown';

function ChoreList({ chores }) {
  const [selChore, setSelChore] = useState(null);

  const [addedChores, setAddedChores] = useState([]);

  const handleSelChore = ({ _id }) => {
    if (selChore === _id) {
      setSelChore(null);
    } else setSelChore(_id);
  };

  return (
    <ul className="divide-y divide-gray-200">
      {chores?.map((chore) => (
        <li key={chore._id}>
          {selChore === chore?._id ? (
            <ChoreListPopDown chore={chore} handleSelChore={handleSelChore} />
          ) : (
            <ChoreListHeader chore={chore} img="plusofficon" handleSelChore={handleSelChore} />
          )}
        </li>
      ))}
    </ul>
  );
}

export default ChoreList;
