import { useState, useEffect } from 'react';
import { useRouter } from 'next/navigation';

const useLocalStorage = (key, initialData) => {
  const router = useRouter();

  const [data, setData] = useState(() => {
    const cachedData = localStorage.getItem(key);

    if (!cachedData) router.push('/');

    return JSON.parse(cachedData);
  });

  useEffect(() => {
    localStorage.setItem(key, JSON.stringify(data));
  }, [key, data]);

  return [data, setData];
};

export default useLocalStorage;
